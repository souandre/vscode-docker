# Microsoft VSCode 

Este projeto apresenta imagens para serem utilizadas no desenvolvimento de sistemas utilizando várias linguagens de programação. Em cada imagem está um ambiente completo instalado para desenvolvimeto de aplicações.

Como cada imagem tem a sua particularidade, existe um arquivo README.md em cada diretório explicando o propósito daquela versão e como criar o container.

Para mais informações e dúvidas, enviar e-amail para souandre.com@gmail.coms